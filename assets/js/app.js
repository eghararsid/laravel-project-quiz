var app = new Vue({
	el: '#app',
	data: {
		nama: '',
		button1: 'Add',
		button2: 'Edit',
		button3: 'Update',
		button4: 'Delete',
		show: 'Add',
		index: [0],
		users: [
			{name: 'Muhammad Iqbal Mubarak'},
			{name: 'Muhammad Attar Hallijendar'},
			{name: 'Muhammad Farel Eghar'}
		]
	},
	methods: {
		newPerson: function () {
			this.users.push({"name" : this.nama});
			this.nama = '';
		},
		changeButton: function (index) {
			this.show = '';
			this.nama = this.users[index]["name"];
			this.index[0] = index;
		},
		updatePerson: function (index) {
			this.users[index]["name"] = this.nama;
			this.nama = '';
			this.show = 'Add';
		},
		deletePerson: function (index) {
			
			if (confirm("Are you sure?")) {
				this.users.splice(index,1);
			}
		}
	}
})